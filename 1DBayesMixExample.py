from sklearn import mixture
from sklearn.externals import joblib
from sklearn import preprocessing

import numpy as np

from scipy.stats import norm, multivariate_normal

import pylab as plt


np.random.seed(1325032)
Ndim=1
N=1000000
Nprime = 1000
var1 = np.random.rand()
var2 = np.random.rand()
print var1, var2


# (1) Draw samples from the true distribution
X = np.concatenate((np.random.normal(0, var1, 0.3 * N),\
        np.random.normal(5, var2, 0.7 * N)))[:, np.newaxis]

# (2) Compute the whitening transformation which makes the samples mean zero, unit covariance (identity matrix) 
# ...this is just to rescale the data (doesn't mean that it was drawn from a mean zero, unit variance multivariate Gaussian

scaler = preprocessing.StandardScaler().fit(X)
X_scaled = scaler.transform(X)
# (3) find the Gaussian mixture model (GMM) that models the transformed data

dpgmm = mixture.BayesianGaussianMixture(n_components=Ndim*2,
                                        covariance_type='full', n_init=1).fit(X_scaled)

## save the model
joblib.dump(dpgmm, '1D_BGMWithScaling.pkl') 
## do some plotting of the true density ##

# (4) make some samples for the comparison for the true density

X_plot = np.linspace(-5, 10, 500)[:, np.newaxis]

true_dens = (0.3 * norm(0, var1).pdf(X_plot[:, 0])
             + 0.7 * norm(5, var2).pdf(X_plot[:, 0]))

# (5) get the transformed data points corresponding to X_plot

X_plot_transformed = scaler.transform(X_plot) 
# compute the log_density of X_plot_transformed
log_density = dpgmm.score_samples(X_plot_transformed)# - np.log(scaler.scale_)
# compute the estimate for the GMM at X_plot_transformed. Note that becase the modelled distribution has variance=1, we need to re-introduce the 1/std = 1/(scale_) factor removed by StandardScaler: scaler.scale_ (note: in higher dimensions, the scaling is det(np.matrix(scaler.scale_))
BayesEstimate = np.exp(log_density)/scaler.scale_
# include the scaler.scale_ from the log_density for completeness:
log_density -= np.log(scaler.scale_)

print (np.log(true_dens) - log_density)/np.log(true_dens) 

fig, ax = plt.subplots()
ax.fill(X_plot, true_dens, fc='black', alpha=0.2,\
	label='input distribution')

ax.plot(X_plot[:, 0], np.exp(log_density), '-',\
        label="GMM")

ax.text(6, 0.38, "N={0} points".format(N))

ax.legend(loc='upper left')
#ax.plot(X[0, 0], -0.005 - 0.01 * np.random.random(X.shape[0]), '+k')

ax.set_xlim(-4, 9)
ax.set_ylim(-0.02, 1)
plt.show()

plt.close()

plt.plot(X_plot[:, 0], np.abs((np.log(true_dens) - log_density)/np.log(true_dens)))
plt.xlabel('x')
plt.ylabel('relative error (log(true density) - log(GMM))/log(true density')
plt.legend()
plt.show()
plt.close()
