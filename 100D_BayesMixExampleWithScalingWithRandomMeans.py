from sklearn import mixture
from sklearn.externals import joblib
from sklearn import preprocessing
from sklearn.decomposition import PCA

import numpy as np

from scipy.stats import norm, multivariate_normal

#import pylab as plt

np.random.seed(23523)

Ndim=100
N=200000
Nprime = 1000

c1 = np.random.rand(Ndim,Ndim)
c2 = np.random.rand(Ndim,Ndim)

C1 = np.dot(c1,c1.T)
C2 = np.dot(c2,c2.T)


means1 = np.random.uniform(-1,1,Ndim)
means2 = np.random.uniform(-5,5,Ndim)

# (1) Draw samples from the true distribution
X = np.array((np.concatenate((np.random.multivariate_normal(means1, C1, size=int(0.3 * N)),\
        np.random.multivariate_normal(means2, C2, size=int(0.7 * N))))))
# (2) Compute the whitening transformation which makes the samples mean zero, unit covariance (identity matrix) 
# ...this is just to rescale the data (doesn't mean that it was drawn from a mean zero, unit variance multivariate Gaussian

scaler = preprocessing.StandardScaler().fit(X)
pca = PCA(n_components=Ndim, svd_solver='full', whiten=True)

X_scaled = pca.fit_transform(X)#scaler.transform(X)

print np.cov(X_scaled.T)
print np.cov(X.T)
# (3) find the Gaussian mixture model (GMM) that models the transformed data

dpgmm = mixture.BayesianGaussianMixture(n_components=Ndim*2,
                                        covariance_type='full', n_init=1).fit(X_scaled)

## save the model
joblib.dump(dpgmm, '100D_BGMWithScalingWithRandomMeans.pkl') 

## do some plotting of the true density ##

# (4) make some samples for the comparison for the true density
X_plot = np.array((np.concatenate((np.random.multivariate_normal(means1, C1, int(0.3 * Nprime)),\
        np.random.multivariate_normal(means2, C2, int(0.7 * Nprime))))))

true_dens = (0.3*multivariate_normal.pdf(X_plot, means1, C1)
             + 0.7*multivariate_normal.pdf(X_plot, means2, C2))
log_true_dens = np.log(true_dens)
# (5) get the transformed data points corresponding to X_plot

X_plot_transformed = pca.transform(X_plot)#scaler.transform(X_plot) 
# compute the log_density of X_plot_transformed
log_density = dpgmm.score_samples(X_plot_transformed)# - np.log(scaler.scale_)
print log_density
# compute the estimate for the GMM at X_plot_transformed. Note that becase the modelled distribution has a different variance, we need to remove the scale factor introducted by the StandardScaler: scaler.scale_
#BayesEstimate = np.exp(log_density)/np.linalg.det(np.diag(scaler.scale_))
BayesEstimate = np.exp(log_density)/np.linalg.det(np.cov(X.T))**(1./2.)
# now remove the scaler.scale_ from the log_density for completeness:
#log_density -= np.log(np.linalg.det(np.diag(scaler.scale_)))
log_density -= np.log(np.linalg.det(np.cov(X.T))**(1./2.))
print (log_true_dens - log_density)/log_true_dens 
joblib.dump(pca, '100D_BGMWithScalingWithRandomMeans_PCA.pkl')
np.save("100D_BGMWithScalingWithRandomMeans_errors", (log_true_dens - log_density)/log_true_dens)

'''
fig, ax = plt.subplots()
ax.fill(X_plot, true_dens, fc='black', alpha=0.2,\
	label='input distribution')

ax.plot(X_plot[:, 0], np.exp(log_density), '-',\
        label="BGM w/ Dirichlet process prior")

ax.text(6, 0.38, "N={0} points".format(N))

ax.legend(loc='upper left')
#ax.plot(X[0, 0], -0.005 - 0.01 * np.random.random(X.shape[0]), '+k')

ax.set_xlim(-4, 9)
ax.set_ylim(-0.02, 1)
plt.show()

plt.close()
'''
